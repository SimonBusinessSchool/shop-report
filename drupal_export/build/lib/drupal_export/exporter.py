import csv, StringIO, urllib

import phpserialize
from flask import (
    Flask,
    render_template,
    request,
    url_for,
    redirect,
    session,
    make_response
)
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.ldap import LDAP, login_required
from werkzeug.serving import run_simple
from werkzeug.wsgi import DispatcherMiddleware

app = Flask(__name__)
app.debug = True
app.secret_key = 'fwagj3wfg744384fbd'
app.config['APPLICATION_ROOT'] = '/simonexport'

app.config['LDAP_SCHEMA'] = 'ldap'
app.config['LDAP_HOST'] = 'its-w2k8-1adc.ur.rochester.edu'
app.config['LDAP_PORT'] = '389'
app.config['LDAP_DOMAIN'] = 'ur.rochester.edu'
app.config['LDAP_SEARCH_BASE'] = 'OU=Simon,DC=UR,DC=Rochester,DC=edu'
app.config['LDAP_REQUIRED_GROUP'] = None
app.config['LDAP_LOGIN_VIEW'] = 'ldap_auth'
app.config['LDAP_LOGIN_TEMPLATE'] = 'auth.html'
app.config['LDAP_SUCCESS_REDIRECT'] = 'index'
ldap = LDAP (app)
app.add_url_rule ('/auth', 'ldap_auth', ldap.login, methods = ['GET', 'POST'])

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqldb://drupalexport:GD6jgruGRT76@localhost/drupal'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy (app)

@app.route ('/')
@login_required
def index ():
    # SELECT op.order_product_id, p.vid, p.model FROM uc_order_products op JOIN uc_products p ON p.nid = op.nid GROUP BY p.vid
    products = db.engine.execute ('SELECT op.order_product_id, p.vid, p.model, COUNT(op.order_product_id) AS amount \
                                   FROM uc_order_products op \
                                   INNER JOIN uc_products p ON p.nid = op.nid \
                                   GROUP BY p.vid')

    return render_template ('index.html', products = products)

@app.route ('/logout')
@login_required
def logout ():
    session.pop ('username', None)
    return redirect (url_for (app.config['LDAP_LOGIN_VIEW']) )

@app.route ('/view/<int:product_id>')
@login_required
def view_product (product_id):
    product = db.engine.execute ('SELECT p.model FROM uc_products p WHERE p.vid = {prod_id}'.format (prod_id = product_id) ).first ()

    db_orders = db.engine.execute ("SELECT op.order_product_id, op.data, op.qty, \
                                           CONCAT('$', FORMAT(op.price, 2) ) AS price, \
                                           FROM_UNIXTIME(o.created, GET_FORMAT(DATETIME,'USA') ) AS created \
                                    FROM uc_products p \
                                    INNER JOIN uc_order_products op ON op.nid = p.nid \
                                    INNER JOIN uc_orders o ON o.order_id = op.order_id \
                                    WHERE p.vid = {prod_id}".format (prod_id = product_id) )

    product_orders = []
    for o in db_orders:
        order = {
            'order_product_id': o.order_product_id,
            'quantity': o.qty,
            'created': o.created,
            'price': o.price,
            'data': phpserialize.loads (o.data)
        }

        product_orders.append (order)

    return render_template ('product.html', product = product, product_orders = product_orders)

@app.route ('/export/<int:product_id>')
@login_required
def export_product (product_id):
    product = db.engine.execute ('SELECT p.model FROM uc_products p WHERE p.vid = {prod_id}'.format (prod_id = product_id) ).first ()

    db_orders = db.engine.execute ("SELECT op.order_product_id, op.data, o.primary_email, \
                                           CONCAT('$', FORMAT(op.price, 2) ) AS price, \
                                           FROM_UNIXTIME(o.created, GET_FORMAT(DATETIME,'USA') ) AS created \
                                    FROM uc_products p \
                                    INNER JOIN uc_order_products op ON op.nid = p.nid \
                                    INNER JOIN uc_orders o ON o.order_id = op.order_id \
                                    WHERE p.vid = {prod_id}".format (prod_id = product_id) )

    sio = StringIO.StringIO ()
    doc = csv.writer (sio)
    temp_rows = []
    headers = []
    headers.append ('Primary Email')
    headers.append ('Price')
    headers.append ('Order Date/Time')

    for o in db_orders:
        extra_data = phpserialize.loads (o.data)

        attrs = []
        attrs.append (o.primary_email)
        attrs.append (o.price)
        attrs.append (o.created)

        # add serialized attributes
        for k, v in extra_data['attributes'].iteritems ():
            if k not in headers:
                headers.append (k)

            # for some reason every attribute is a dict with one element
            attrs.append (v.itervalues ().next () )

        temp_rows.append (attrs)

    doc.writerow (list (headers) )
    for r in temp_rows:
        doc.writerow (r)

    # clean up filename to insert into header
    csv_file = urllib.quote (str (product.model) )
    output = make_response (sio.getvalue () )
    output.headers['Content-Disposition'] = 'attachment; filename=' + csv_file + '.csv'
    output.headers['Content-type'] = 'text/csv'
    return output

parent_app = DispatcherMiddleware (app, {'/simonexport': app})

if __name__ == '__main__':
    #app.run(host = '127.0.0.1', port = 22000, debug = True)
    run_simple ('localhost', 22000, parent_app)
