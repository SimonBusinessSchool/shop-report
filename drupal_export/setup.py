from setuptools import setup, find_packages

from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='drupal_export',
    version='1.0',
    description='Exports ubercart orders from drupal',
    long_description=long_description,
    keywords='drupal ubercart simon',
    packages=['drupal_export'],
    zip_safe=False,
    include_package_data=True,

    install_requires=['Flask==0.10.1',
                      'Flask-LDAP==0.1.6',
                      'Flask-SQLAlchemy==2.1',
                      'gevent==1.1rc1',
                      'MySQL-Python==1.2.5',
                      'phpserialize==1.3']
)
